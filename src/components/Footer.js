import React from 'react';
import { Icon, Layout } from 'antd';

const { Footer } = Layout;

const styles = {
  footer: {
    color: '#ccc',
    textAlign: 'center',
  }
}

const FooterComponent = () => (
  <Footer style={styles.footer}>
    <a href="https://gitlab.com/yavoo/zonky-app.git">
      <Icon type="gitlab" />
      https://gitlab.com/yavoo/zonky-app.git
    </a>
  </Footer>
);

export default FooterComponent;
