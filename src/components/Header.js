import React from 'react';
import { Icon, Layout } from 'antd';

const { Header: AntHeader } = Layout;

const styles = {
  header: {
    color: 'white',
  },
  icon: {
    fontSize: 24,
    marginRight: '12px',
    verticalAlign: 'middle'
  }
}

const Header = () => (
  <AntHeader style={styles.header}>
    <Icon style={styles.icon} type="dot-chart" />
    Average Loans
  </AntHeader>
);

export default Header;
