import { Card, Icon } from 'antd';
import currencyFormatter from 'currency-formatter';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { getLabel } from '../lib/ratings';

class Results extends Component {
  render() {
    const { average, loading, rating } = this.props;
    const title = rating && !loading ? `Rating ${getLabel(rating)}` : false;

    return (
      <Card style={{ textAlign: 'center' }} title={title}>
        {loading ?
          <Icon type="loading" style={{ fontSize: 32, color: '#ccc' }} />
          :
          average ?
            <p>Average Amount: <b>{currencyFormatter.format(average, { locale: 'cs-CZ' })}</b></p>
            :
            <Icon type="dot-chart" style={{ fontSize: 32, color: '#ccc' }} />
        }
      </Card>
    );
  }
}

Results.propTypes = {
  average: PropTypes.number,
  loading: PropTypes.bool,
  rating: PropTypes.string,
};

const mapStateToProps = state => ({
  average: state.stats.average,
  loading: state.stats.loading,
  rating: state.stats.rating,
});

export default connect(
  mapStateToProps
)(Results);
