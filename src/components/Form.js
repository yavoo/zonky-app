import { Button, Form } from 'antd';
import React, { Component } from 'react';
import { Field, reduxForm, formValueSelector } from 'redux-form'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getStats } from '../actions/stats';
import RatingInput from './RatingInput';

const FormItem = Form.Item;
const required = value => value ? undefined : 'Required';

class FormComponent extends Component {
  handleSubmit = (e) => {
    const { rating, getStats } = this.props;
    e.preventDefault();
    getStats(rating);
  }

  render() {
    const { pristine, submitting } = this.props;

    return (
      <Form layout="vertical" onSubmit={this.handleSubmit}>
        <h2>Count The Average Loans</h2>
        <FormItem label="Rating">
          <Field component={RatingInput} name="rating" placeholder="asfa" validate={[required]} />
        </FormItem>
        <FormItem>
          <Button disabled={pristine || submitting} htmlType="submit" type="primary">Count It!</Button>
        </FormItem>
      </Form>
    );
  }
}

const selector = formValueSelector('stats');
const createReduxForm = reduxForm({ form: 'stats' })

FormComponent.propTypes = {
  getStats: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  rating: selector(state, 'rating'),
});

const mapDispatchToProps = dispatch => ({
  getStats: (rating) => dispatch(getStats(rating)),
});


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(createReduxForm(FormComponent));
