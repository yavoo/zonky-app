import { Radio } from 'antd';
import React, { Component } from 'react';

import { ratings } from '../lib/ratings';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class RatingInput extends Component {
  render() {
    const { onChange } = this.props.input;

    return (
      <RadioGroup onChange={onChange} size="small">
        {ratings.map(r => <RadioButton key={r.value} value={r.value}>{r.label}</RadioButton>)}
      </RadioGroup>
    );
  }
}

export default RatingInput;
