import { applyMiddleware, createStore as createReduxStore, combineReducers, compose } from 'redux';
import { reducer as formReducer } from 'redux-form'
import logger from 'redux-logger';
import thunk from 'redux-thunk';

import promiseMiddleware from './lib/promiseMiddleware';

// Reducers
import { statsReducer } from './reducers/stats';

const createStore = (initialState = {}) => {
  const reducers = {
    form: formReducer,
    stats: statsReducer,
  };

  const customMiddlewares = applyMiddleware(
    thunk,
    promiseMiddleware,
    logger
  );

  const allMiddlewares = process.env.IS_BROWSER && window.devToolsExtension
    ? compose(customMiddlewares, window.devToolsExtension())
    : customMiddlewares;

  return createReduxStore(
    combineReducers(reducers),
    initialState,
    allMiddlewares
  );
};

export default createStore;
