import { Col, Layout, Row } from 'antd';
import React, { Component } from 'react';
import { Provider } from 'react-redux';

import Footer from './components/Footer';
import Form from './components/Form';
import GoogleTagManager from './components/GoogleTagManager';
import Header from './components/Header';
import Results from './components/Results';
import createStore from './createStore';

import './App.sass';

const { Content } = Layout;

const store = createStore();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Layout style={{ height: '100vh' }}>
          <Header />
          <Content style={{ padding: '32px 0' }}>
            <Row>
              <Col xs={{ span: 20, offset: 2 }} md={{ span: 16, offset: 4 }}>
                <Row gutter={32}>
                  <Col md={14}>
                    <Form />
                  </Col>
                  <Col md={10}>
                    <Results />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Content>
          <Footer />
        <GoogleTagManager gtmId='GTM-W6BRKVZ' />
        </Layout>
      </Provider>
    );
  }
}

export default App;
