import { message } from 'antd';
import qs from 'qs';

import average from '../lib/average';

export const GET_STATS = 'GET_STATS';
export const GET_STATS_START = 'GET_STATS_START';
export const GET_STATS_SUCCESS = 'GET_STATS_SUCCESS';
export const GET_STATS_ERROR = 'GET_STATS_ERROR';

export const getStats = (rating) => {
  const query = {
    fields: 'amount',
    rating__eq: rating,
  }
  const promise = fetch(`/api/loans/marketplace?${qs.stringify(query)}`)
    .then(response => response.json())
    .then(json => json.map(loan => loan.amount))
    .then(average)
    .then(amount => ({
        average: amount,
        rating,
    }))
    .catch(error => {
      message.error('Zonky API error', 5);
    });

  return {
    type: GET_STATS,
    payload: promise
  };
};

