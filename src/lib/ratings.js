import * as R from 'ramda';

export const ratings = [
  {
    label: "A**",
    value: "AAAAA",
  },
  {
    label: "A*",
    value: "AAAA",
  },
  {
    label: "A++",
    value: "AAA",
  },
  {
    label: "A+",
    value: "AA",
  },
  {
    label: "A",
    value: "A",
  },
  {
    label: "B",
    value: "B",
  },
  {
    label: "C",
    value: "C",
  },
  {
    label: "D",
    value: "D",
  }
]

export const getLabel = (value) => R.find(R.propEq('value', value))(ratings).label;
