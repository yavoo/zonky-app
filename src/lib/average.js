import * as R from 'ramda';

export const average = R.converge(R.divide, [R.sum, R.length]);

export default average;
