import * as actions from '../actions/stats';

export const statsInitialState = {
  average: 0,
  loading: false,
  rating: '',
};

export function statsReducer(state = statsInitialState, action) {
  switch (action.type) {
    case actions.GET_STATS_START:
      return { ...state, loading: true };

      case actions.GET_STATS_SUCCESS:
      const { average, rating } = action.payload;
      return { ...state, average, loading: false, rating };

    default:
      return state;
  }
}
